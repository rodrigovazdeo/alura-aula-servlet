Data da documentação: 05/10/2021.

# como subir a aplicação para testes


Baixa a versão do tomcat vesão 9.0.53:
	
`` $ wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.53/bin/apache-tomcat-9.0.53.tar.gz ``
	
Para extrair os arquivos:

`` $ tar -xf apache-tomcat-9.0.53.tar.gz ``
	
Gere o war no eclipse.

Copia o arquivo war na pasta webapp no seu dentro do tomcat:

`` $ cp meuarquivo.war apache-tomcat-9.0.53/webapps ``

Após isso, mude de pasta para bin dentro do tomcat:

`` $ cd apache-tomcat-9.0.53/bin ``

E execute o tomcat:

`` $ ./startup.sh ``

Pronto, agora só testar os endpoints criados.


