package br.dev.rvz.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter("/entrada")
public class AutorizationFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		System.out.println("AutorizationFilter");
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		Boolean notExistUserSession = (httpServletRequest.getSession().getAttribute("usuarioLogado") == null);
		Boolean isProtectedAction = !(request.getParameter("acao").equals("entrar")
				|| request.getParameter("acao").equals("autenticar"));

		if (isProtectedAction & notExistUserSession) {
			httpServletResponse.sendRedirect("entrada?acao=entrar");
			return;
		}
		// pass the request along the filter chain
		chain.doFilter(httpServletRequest, httpServletResponse);
	}

}
