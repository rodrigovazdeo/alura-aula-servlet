package br.dev.rvz.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.actions.AtualizarEmpresa;
import br.dev.rvz.actions.AutenticarUsuario;
import br.dev.rvz.actions.Context;
import br.dev.rvz.actions.EntrarLogin;
import br.dev.rvz.actions.InserirEmpresa;
import br.dev.rvz.actions.ListaEmpresa;
import br.dev.rvz.actions.Logout;
import br.dev.rvz.actions.NovaEmpresa;
import br.dev.rvz.actions.RemoverEmpresa;

public class ControllerFilter implements Filter {

	private Context context;
	private String view = "";

	public ControllerFilter() {
		iniciarServlets();
	}

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		this.view = this.context.execute(request.getParameter("acao"), httpServletRequest, httpServletResponse);
		changeView(httpServletRequest, httpServletResponse);
	}

	private void iniciarServlets() {
		this.context = new Context();
		this.context.setContext("listaEmpresa", new ListaEmpresa()).setContext("novaEmpresa", new NovaEmpresa())
				.setContext("atualizarEmpresa", new AtualizarEmpresa())
				.setContext("removerEmpresa", new RemoverEmpresa())
				.setContext("inserirNovaEmpresa", new InserirEmpresa()).setContext("entrar", new EntrarLogin())
				.setContext("autenticar", new AutenticarUsuario()).setContext("logout", new Logout());
	}

	private void changeView(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String[] nameViews = view.split(":");

		if ("forward".equals(nameViews[0])) {
			RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/view/" + nameViews[1]);
			requestDispatcher.forward(request, response);
		} else {
			response.sendRedirect(nameViews[1]);
		}
	}
}
