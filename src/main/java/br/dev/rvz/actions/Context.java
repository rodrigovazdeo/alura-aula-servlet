package br.dev.rvz.actions;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;

public class Context {

	private HashMap<String, ActionServlet> contexts = new HashMap<String, ActionServlet>();

	public Context setContext(String actionName, ActionServlet context) {
		this.contexts.put(actionName, context);
		return this;
	}

	public String execute(String action, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		return this.contexts.get(action).run(request, response);
	}
}