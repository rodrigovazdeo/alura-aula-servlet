package br.dev.rvz.actions;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;
import br.dev.rvz.models.Empresa;
import br.dev.rvz.repository.Banco;

public class NovaEmpresa implements ActionServlet {

	private final Banco banco = new Banco();

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Empresa empresa = new Empresa();
		empresa.setNome(request.getParameter("nomeCompleto"));

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

		try {
			Date dataAbertura = simpleDateFormat.parse(request.getParameter("dataAbertura"));
			empresa.setDataAbertura(dataAbertura);
		} catch (ParseException e) {
			throw new ServletException(e);
		}

		banco.add(empresa);

		return "redirect:entrada?acao=listaEmpresa";
	}

}
