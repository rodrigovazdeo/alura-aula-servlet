package br.dev.rvz.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;
import br.dev.rvz.models.Empresa;
import br.dev.rvz.repository.Banco;

public class AtualizarEmpresa implements ActionServlet {

	private final Banco banco = new Banco();

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		if ("GET".equals(request.getMethod())) {
			Empresa empresa = banco.buscarPorId(Integer.valueOf(request.getParameter("id")));

			request.setAttribute("empresa", empresa);
			return "forward:formAtualizarEmpresa.jsp";
		} else if ("POST".equals(request.getMethod())) {
			Empresa empresa = banco.buscarPorId(Integer.valueOf(request.getParameter("id")));
			empresa.setNome(request.getParameter("nomeCompleto"));

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

			try {
				Date dataAbertura = simpleDateFormat.parse(request.getParameter("dataAbertura"));
				empresa.setDataAbertura(dataAbertura);
			} catch (ParseException e) {
				throw new ServletException(e);
			}

			return "redirect:entrada?acao=listaEmpresa";
		} else {
			return null;
		}
	}
}
