package br.dev.rvz.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import br.dev.rvz.endpoints.ActionServlet;
import br.dev.rvz.models.User;
import br.dev.rvz.services.UserService;

public class AutenticarUsuario implements ActionServlet {

	private final UserService userService = new UserService();

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		User user = new User();
		user.setUserName(request.getParameter("username"));
		user.setPasword(request.getParameter("password"));

		if (userService.isEqual(user)) {
			HttpSession httpSession = request.getSession();
			httpSession.setAttribute("usuarioLogado", user.getUserName());
			return "redirect:entrada?acao=listaEmpresa";
		}

		return "forward:formLogin.jsp";
	}

}
