package br.dev.rvz.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;
import br.dev.rvz.repository.Banco;

public class RemoverEmpresa implements ActionServlet {

	private final Banco banco = new Banco();

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer id = Integer.parseInt(request.getParameter("id"));
		banco.deleteById(id);

		return "redirect:entrada?acao=listaEmpresa";
	}

}
