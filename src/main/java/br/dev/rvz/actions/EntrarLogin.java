package br.dev.rvz.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;

public class EntrarLogin implements ActionServlet {

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		return "forward:formLogin.jsp";
	}

}
