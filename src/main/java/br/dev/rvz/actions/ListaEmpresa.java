package br.dev.rvz.actions;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.dev.rvz.endpoints.ActionServlet;
import br.dev.rvz.models.Empresa;
import br.dev.rvz.repository.Banco;

public class ListaEmpresa implements ActionServlet {

	private final Banco banco = new Banco();

	@Override
	public String run(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Iterable<Empresa> empresas = banco.getEmpresas();
		request.setAttribute("empresas", empresas);

		return "forward:listaEmpresa.jsp";
	}
}