package br.dev.rvz.repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.dev.rvz.models.Empresa;
import br.dev.rvz.models.User;

public class Banco {

	private final static List<Empresa> empresas = new ArrayList<Empresa>();
	private final static List<User> users = new ArrayList<User>();
	private static Integer sequencialId = 1;

	static {
		Empresa empresa = new Empresa();
		empresa.setId(sequencialId++);
		empresa.setNome("Alura");

		Empresa empresa2 = new Empresa();
		empresa2.setId(sequencialId++);
		empresa2.setNome("DevMedia");

		empresas.addAll(Arrays.asList(empresa, empresa2));

		User user1 = new User();
		user1.setUserName("rodrigovaz@gmail.com");
		user1.setPasword("12345678");

		User user2 = new User();
		user2.setUserName("anaz@gmail.com");
		user2.setPasword("12345678");

		users.addAll(Arrays.asList(user1, user2));
	}

	public void add(Empresa empresa) {
		empresa.setId(sequencialId++);
		empresas.add(empresa);
	}

	public Iterable<Empresa> getEmpresas() {
		return Banco.empresas;
	}

	public void deleteById(Integer id) {
		empresas.removeIf(empresa -> id.equals(empresa.getId()));
	}

	public Empresa buscarPorId(Integer id) {
		for (Empresa empresa : empresas) {
			if (id.equals(empresa.getId())) {
				return empresa;
			}
		}

		return null;
	}

	public static List<User> getUsers() {
		return users;
	}

}