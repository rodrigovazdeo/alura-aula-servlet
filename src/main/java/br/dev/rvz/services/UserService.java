package br.dev.rvz.services;

import br.dev.rvz.models.User;
import br.dev.rvz.repository.Banco;

public class UserService {

	public boolean isEqual(User user) {
		try {
			User userAutentication = findUserByUserName(user.getUserName());
			return userAutentication.getPasword().equals(user.getPasword());
		} catch (RuntimeException e) {
			System.out.println(e.getMessage());
			return false;
		}

	}

	private User findUserByUserName(String userName) {
		User userFound = null;
		for (User user : Banco.getUsers()) {
			if (userName.equals(user.getUserName())) {
				userFound = user;
			}
		}

		if (userFound == null) {
			throw new RuntimeException("Usuário não localizado com username = " + userName);
		}

		return userFound;
	}
}
