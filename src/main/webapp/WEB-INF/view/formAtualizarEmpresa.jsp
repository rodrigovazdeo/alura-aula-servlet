<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<c:url value="/entrada?acao=atualizarEmpresa"
	var="linkServletAtualizarEmpresa"></c:url>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:import url="logout-partial.jsp" />
	<form name="formAtualizarEmpresa" id="formAtualizarEmpresa"
		method="post" action="${ linkServletAtualizarEmpresa }">
		<label>Nome Completo:</label> <input type="text" name="nomeCompleto"
			id="nomeCompleto" value="${ empresa.nome }"
			placeholder="digite o nome completo ..." /> <label>Data
			Abertura:</label> <input type="text"
			value="<fmt:formatDate
					value="${ empresa.dataAbertura }" pattern="dd/MM/yyyy" />"
			name="dataAbertura" id="dataAbertura"
			placeholder="digite de abertura ..." /> <input id="id" name="id"
			type="hidden" value="${ empresa.id }" />
		<button type="submit" name="btnCadastrar" id="btnCadastrar">Atualizar</button>
		<button type="reset" name="btnLimpar" id="btnLimpar">limpar</button>
	</form>
</body>
</html>