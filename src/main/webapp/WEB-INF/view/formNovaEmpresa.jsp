<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:url value="/entrada?acao=novaEmpresa" var="linkServletNovaEmpresa"></c:url>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<c:import url="logout-partial.jsp" />
	<form name="formNovaEmpresa" id="formNovaEmpresa" method="post"
		action="${ linkServletNovaEmpresa }">
		<label>Nome Completo:</label> <input type="text" name="nomeCompleto"
			id="nomeCompleto" placeholder="digite o nome completo ..." /> <label>Data
			Abertura:</label> <input type="text" name="dataAbertura" id="dataAbertura"
			placeholder="digite de abertura ..." />

		<button type="submit" name="btnCadastrar" id="btnCadastrar">cadastrar</button>
		<button type="reset" name="btnLimpar" id="btnLimpar">limpar</button>
	</form>
</body>
</html>