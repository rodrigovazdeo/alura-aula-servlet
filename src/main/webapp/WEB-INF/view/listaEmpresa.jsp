<%@ page import="br.dev.rvz.models.Empresa"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Usuário logado: ${ usuarioLogado }</h1>
	<c:if test="${ not empty empresa }">
		<p>Nova empresa ${ empresa } cadastrada com sucesso!</p>
	</c:if>
	<a href="/gerenciador/entrada?acao=inserirNovaEmpresa" title="Nova empresa">Nova empresa</a>
	<c:import url="logout-partial.jsp" />
	<ul>
		<c:forEach items="${ empresas }" var="empresa">
			<li> ${ empresa.id } |  ${ empresa.nome } | <fmt:formatDate
					value="${ empresa.dataAbertura }" pattern="dd/MM/yyyy" /> 
					<a href="/gerenciador/entrada?acao=atualizarEmpresa&id=${ empresa.id }">atualizar</a> 
					<a href="/gerenciador/entrada?acao=removerEmpresa&id=${ empresa.id }">Remover</a>
			</li>
		</c:forEach>
	</ul>
</body>
</html>	