<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:url value="/entrada?acao=autenticar" var="aunteticarUsuario"></c:url>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Entrada</title>
</head>
<body>
	<form method="POST" name="formLogin" id="formLogin"
		action="${ aunteticarUsuario }">
		<label for="username"> Usuario: </label> <input type="text"
			name="username" id="username" placeholder="digite seu usuário"
			required /> <label for="password"> senha: </label> <input
			type="password" name="password" id="password"
			placeholder="digite sua senha" required /> <br />
		<button name="btnEnter" id="btnEnter" type="submit">Entrar</button>
		<button name="btnLimpar" id="btnLimpar" type="reset">Limpar</button>
	</form>
</body>
</html>