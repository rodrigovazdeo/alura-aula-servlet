<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Listar empresas</title>
</head>
<body>
	<c:import url="logout-partial.jsp" />
	<c:if test="${ not empty empresa }">
		<p>Nova empresa ${ empresa } cadastrada com sucesso!</p>
	</c:if>
	<c:if test="empty empresa">
		<p>nenhuma empresa cadastrada</p>
	</c:if>
</body>
</html>